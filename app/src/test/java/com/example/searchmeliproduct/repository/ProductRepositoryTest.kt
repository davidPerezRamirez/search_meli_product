package com.example.searchmeliproduct.repository

import com.example.searchmeliproduct.apiservice.MLService
import com.example.searchmeliproduct.helpers.TypeSearch
import com.example.searchmeliproduct.model.ListProduct
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test
import retrofit2.Call

class ProductRepositoryTest {

    private val mockService = mockk<MLService>()
    private val mockCall = mockk<Call<ListProduct>>()

    private lateinit var productRepository: ProductRepository
    private lateinit var valueToSearch: String
    private lateinit var typeOfSearch: TypeSearch

    @Before
    fun setUp() {
        productRepository = ProductRepository(mockService)

        every { mockService.getProductsByCategory(any()) } returns mockCall
        every { mockService.getProductsByName(any()) } returns mockCall
        justRun { mockCall.enqueue(any()) }
    }

    @Test
    fun givenWantGetProductsByCategoryThenGetProductsByCategoryFromMLService () {
        givenWantGetProductsByCategory()

        whenGetProductsByCategory()

        thenGetProductsByCategoryFromMLService()
    }

    @Test
    fun givenWantGetProductsByProductNameThenGetProductsByNameFromMLService () {
        givenWantGetProductsByProductName()

        whenGetProductsByCategory()

        thenGetProductsByNameFromMLService()
    }

    private fun givenWantGetProductsByProductName() {
        this.valueToSearch = "name product"
        this.typeOfSearch = TypeSearch.NAME_PRODUCT
    }

    private fun givenWantGetProductsByCategory() {
        this.valueToSearch = "MLA123"
        this.typeOfSearch = TypeSearch.CATEGORY
    }

    private fun whenGetProductsByCategory() {
        productRepository.products(this.valueToSearch, this.typeOfSearch)
    }

    private fun thenGetProductsByCategoryFromMLService() {
        verify(exactly = 1) {
            mockService.getProductsByCategory(valueToSearch)
        }
    }

    private fun thenGetProductsByNameFromMLService() {
        verify (exactly = 1) {
            mockService.getProductsByName(valueToSearch)
        }
    }

}
