package com.example.searchmeliproduct.repository

import com.example.searchmeliproduct.apiservice.MLService
import com.example.searchmeliproduct.model.Category
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test
import retrofit2.Call

class CategoryRepositoryTest {

    private val mockService = mockk<MLService>()
    private val mockCall = mockk<Call<List<Category>>>()

    private lateinit var categoryRepository: CategoryRepository

    @Before
    fun setUp() {
        categoryRepository = CategoryRepository(mockService)

        every { mockService.getCategories() } returns mockCall
        justRun { mockCall.enqueue(any()) }
    }

    @Test
    fun whenGetCategoriesThenVerifyCAllMLService () {

        whenGetCategories()

        thenVerifyCAllMLService()
    }

    private fun whenGetCategories() {
        categoryRepository.getCategories()
    }

    private fun thenVerifyCAllMLService() {
        verify(exactly = 1) {
            mockService.getCategories()
        }
    }
}
