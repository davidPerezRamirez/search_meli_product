package com.example.searchmeliproduct.adapter

import com.example.searchmeliproduct.model.Category
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class RecyclerCategoryAdapterTest {

    companion object {
        const val ID_CATEGORY_FOOD = "MLAFood"
        const val NAME_CATEGORY_FOOD = "Alimentos"
    }

    private lateinit var adapter: RecyclerCategoryAdapter
    private var sizeListCategories: Int = 0
    private lateinit var categories: List<Category>

    @Before
    fun setUp() {
        this.adapter = RecyclerCategoryAdapter()

        categories = listOf(
            Category("MLAAuto", "Autos"),
            Category("MLAPC", "Notebook"),
            Category(ID_CATEGORY_FOOD, NAME_CATEGORY_FOOD))
    }

    @Test
    fun whenGetCountOfCategoriesThenReturnSizeEqual3() {
        givenThatExistCategories()

        whenGetCount()

        thenReturnSizeEqual3()
    }

    @Test
    fun  givenThaThereAreNoExistCategoryWhenGetCountOfCategoriesThenReturnSizeEqual0() {

        whenGetCount()

        thenReturnSizeEqual0()
    }

    private fun givenThatExistCategories() {

        adapter.categories = categories
    }

    private fun whenGetCount() {
        sizeListCategories = adapter.itemCount
    }

    private fun thenReturnSizeEqual3() {
        assertThat(sizeListCategories, equalTo(3))
    }

    private fun thenReturnSizeEqual0() {
        assertThat(sizeListCategories, equalTo(0))
    }
}
