package com.example.searchmeliproduct.apiservice

import com.example.searchmeliproduct.model.Category
import com.example.searchmeliproduct.model.ListProduct
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class MLServiceTest {

    private lateinit var responseGetCategories: Response<List<Category>>
    private lateinit var responseGetProducts: Response<ListProduct>
    private lateinit var service: MLService
    private lateinit var idCategory: String
    private lateinit var productName: String

    @Before
    fun setUp() {
        service = ServiceConnector.connect()
    }

    @Test
    fun givenThatGetCategoriesThenReturnListCategoriesNotEmpty() {

        whenGetCategories()

        thenReturnListCategoriesIsNotEmpty()
    }

    @Test
    fun givenThatGetProductsFromAgroCategoryThenReturnListProductsNotEmpty() {

        givenThatCategoryToSearchIsAgro()

        whenGetProductsByCategory()

        thenReturnListProductsIsNotEmpty()
    }

    @Test
    fun givenThatGetProductsFromInvalidCategoryThenReturnListProductsIsEmpty() {

        givenThatCategoryToSearchInvalid()

        whenGetProductsByCategory()

        thenReturnListProductsIsEmpty()
    }

    @Test
    fun givenThatGetProductsByNameRouserThenReturnListProductsNotEmpty() {

        givenThatNameProductIsRouser()

        whenGetProductsByName()

        thenReturnListProductsIsNotEmpty()
    }

    @Test
    fun givenThatGetProductsByInvalidNameThenReturnListProductsIsEmpty() {

        givenThatNameProductIsInvalid()

        whenGetProductsByName()

        thenReturnListProductsIsEmpty()
    }

    private fun givenThatNameProductIsInvalid() {
        this.productName = "%$#/$&#("
    }

    private fun givenThatNameProductIsRouser() {
        this.productName = "Rouser"
    }

    private fun givenThatCategoryToSearchInvalid() {
        idCategory = INVALID_CATEGORY_ID
    }

    private fun givenThatCategoryToSearchIsAgro() {
        idCategory = ID_CATEGORY_AGRO
    }

    private fun whenGetCategories() {
        responseGetCategories = service.getCategories().execute()
    }

    private fun whenGetProductsByCategory() {
        responseGetProducts = service.getProductsByCategory(this.idCategory).execute()
    }

    private fun whenGetProductsByName() {
        responseGetProducts = service.getProductsByName(this.productName).execute()
    }

    private fun thenReturnListCategoriesIsNotEmpty() {
        val categories = responseGetCategories.body()

        assertThat(this.responseGetCategories.code(), equalTo(200))
        assertThat(categories, hasSize(greaterThan(0)))
    }

    private fun thenReturnListProductsIsNotEmpty() {
        val products = responseGetProducts.body()?.results

        assertThat(this.responseGetProducts.code(), equalTo(200))
        assertThat(products, hasSize(greaterThan(0)))
    }

    private fun thenReturnListProductsIsEmpty() {
        val products = responseGetProducts.body()?.results

        assertThat(this.responseGetProducts.code(), equalTo(200))
        assertThat(products, hasSize(0))
    }

    companion object {
        private const val ID_CATEGORY_AGRO = "MLA1512"
        private const val  INVALID_CATEGORY_ID = "invalid category"
    }
}