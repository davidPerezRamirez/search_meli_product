package com.example.searchmeliproduct

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.searchmeliproduct.adapter.RecyclerCategoryAdapter
import com.example.searchmeliproduct.ui.MainActivity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4ClassRunner::class)
class ListProductFragmentTest {

    @Before
    fun setUp(){
        launchActivity<MainActivity>()
    }

    @Test
    fun checkProductsFromSelectedCategoriesIsDisplayed() {
        Thread.sleep(500)
        onView(withId(R.id.rv_categories))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerCategoryAdapter.CategoryViewHolder>(1,
                        ViewActions.click()
                    )
            )
    }
}