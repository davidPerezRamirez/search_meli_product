package com.example.searchmeliproduct

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.searchmeliproduct.ui.MainActivity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4ClassRunner::class)
class CategoriesFragmentTest {

    @Before
    fun setUp(){
        launchActivity<MainActivity>()
    }

    @Test
    fun checkTitleCategoryIsDisplayed(){
        onView(withId(R.id.tv_titleCategory))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkBtnCategoriesIsDisplayed(){
        onView(withId(R.id.btn_categories))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkBtnSearchIsDisplayed(){
        onView(withId(R.id.btn_search))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkListCategoriesIsDisplayed() {
        onView(withId(R.id.rv_categories))
            .check(matches(isDisplayed()))
    }

}