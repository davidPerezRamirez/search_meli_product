package com.example.searchmeliproduct

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.searchmeliproduct.adapter.RecyclerCategoryAdapter
import com.example.searchmeliproduct.adapter.RecyclerListProductAdapter
import com.example.searchmeliproduct.ui.MainActivity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4ClassRunner::class)
class DetailProductFragmentTest {

    @Before
    fun setUp(){
        launchActivity<MainActivity>()
        this.gotDetailProductFragment()
    }

    @Test
    fun checkDetailsProductAreDisplayed() {

        onView(withId(R.id.iv_photoProduct))
            .check(ViewAssertions.matches(isDisplayed()))

        onView(withId(R.id.tv_titleProduct))
            .check(ViewAssertions.matches(isDisplayed()))

        onView(withId(R.id.tv_priceProdcut))
            .check(ViewAssertions.matches(isDisplayed()))

    }

    private fun gotDetailProductFragment() {
        Thread.sleep(500)
        onView(withId(R.id.rv_categories))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerCategoryAdapter.CategoryViewHolder>(1,
                        ViewActions.click()
                    )
            )

        Thread.sleep(200)
        onView(withId(R.id.rv_products))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerListProductAdapter.ProductViewHolder>(1,
                        ViewActions.click()
                    )
            )
    }
}