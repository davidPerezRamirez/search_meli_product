package com.example.searchmeliproduct

import android.view.View
import android.widget.TextView
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.searchmeliproduct.ui.MainActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matcher
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    companion object {
        private const val VALID_NAME_PRODUCT = "Rouser ns 200"
    }

    @Before
    fun setUp(){
        launchActivity<MainActivity>()
    }

    @Test
    fun checkTitleCategoryIsDisplayed(){
        onView(withId(R.id.tv_titleCategory))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkBtnCategoriesIsDisplayed(){
        onView(withId(R.id.btn_categories))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkBtnSearchIsDisplayed(){
        onView(withId(R.id.btn_search))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkEditViewToSearchIsDisplayed(){
        onView(withId(R.id.ev_nameProduct))
            .check(matches(isDisplayed()))
    }

    fun whenSearchChangeTextInEditViewFromNameProductWithValidTextAndClickSearchBtnThenGoToProductsView() {
        onView(withId(R.id.ev_nameProduct))
            .perform(setTextInTextView(VALID_NAME_PRODUCT))

        onView(withId(R.id.btn_search))
            .perform(click())

        onView(withId(R.id.rv_products))
            .check(matches(not(isDisplayed())))
    }

    private fun setTextInTextView(value: String?): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return allOf(isDisplayed(), isAssignableFrom(TextView::class.java))
            }

            override fun perform(uiController: UiController?, view: View) {
                (view as TextView).text = value
            }

            override fun getDescription(): String {
                return "replace text"
            }
        }
    }
}