package com.example.searchmeliproduct.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.searchmeliproduct.helpers.TypeSearch
import com.example.searchmeliproduct.repository.ProductRepository
import com.example.searchmeliproduct.viewModel.ListProductViewModel

@Suppress("UNCHECKED_CAST")
class ListProductViewModelFactory(
    private val productRepository: ProductRepository,
    private val idCategory: String,
    private val typeSearch: TypeSearch
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListProductViewModel(productRepository, idCategory, typeSearch) as T
    }
}