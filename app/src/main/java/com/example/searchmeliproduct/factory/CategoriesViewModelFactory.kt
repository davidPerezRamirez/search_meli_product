package com.example.searchmeliproduct.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.searchmeliproduct.repository.CategoryRepository
import com.example.searchmeliproduct.viewModel.CategoriesViewModel

@Suppress("UNCHECKED_CAST")
class CategoriesViewModelFactory(private val categoryRepository: CategoryRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoriesViewModel(categoryRepository) as T
    }

}