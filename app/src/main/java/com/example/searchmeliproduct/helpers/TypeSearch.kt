package com.example.searchmeliproduct.helpers

enum class TypeSearch {

    CATEGORY,
    NAME_PRODUCT
}