package com.example.searchmeliproduct.helpers

object SearchProductConstants {

    const val TYPE_SEARCH = "typeSearch"
    const val VALUE_TO_SEARCH = "valueToSearch"
}