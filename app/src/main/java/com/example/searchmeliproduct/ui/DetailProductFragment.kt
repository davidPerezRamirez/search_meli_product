package com.example.searchmeliproduct.ui

import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.searchmeliproduct.R
import com.example.searchmeliproduct.model.Product
import com.squareup.picasso.Picasso
import java.lang.StringBuilder
import java.text.NumberFormat
import java.util.*

class DetailProductFragment: Fragment() {

    private lateinit var productSelected: Product

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        this.productSelected = this.requireArguments().getSerializable("productSelected") as Product
        val view = inflater.inflate(R.layout.detail_product_fragment, container, false)

        setUpViews(view)
        val tableLayoutFactory = TableLayoutAttributesProductFactory(this.requireContext(), productSelected.attributes)
        tableLayoutFactory.setUpTableLayout(view)

        return view
    }

    private fun setUpViews(view: View) {
        val linkToSellerProfile = this.productSelected.seller.permalink
        val address = StringBuilder()

        view.findViewById<TextView>(R.id.tv_titleProduct).text = this.productSelected.title
        Picasso.get().load(this.productSelected.thumbnail).resize(300,280).into(view.findViewById<ImageButton>(R.id.iv_photoProduct))
        view.findViewById<TextView>(R.id.tv_priceProdcut).text = formatPrice(this.productSelected.price)

        if(!linkToSellerProfile.isNullOrEmpty()) {
            view.findViewById<TextView>(R.id.tv_linkToSellerProfile).text = linkToSellerProfile
        }
        view.findViewById<CheckBox>(R.id.chk_mercadoPago).isChecked = this.productSelected.accepts_mercadopago
        address.append(this.productSelected.address.city_name).append(", ").append(this.productSelected.address.state_name)
        view.findViewById<TextView>(R.id.tv_address).text = address.toString()
    }


    private fun formatPrice(price: Number): String {
        val countryCode = "AR"
        val languageCoe = "es"

        return NumberFormat.getCurrencyInstance(Locale(languageCoe, countryCode)).format(price)
    }

}