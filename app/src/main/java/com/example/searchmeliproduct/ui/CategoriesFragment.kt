package com.example.searchmeliproduct.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.searchmeliproduct.R
import com.example.searchmeliproduct.adapter.RecyclerCategoryAdapter
import com.example.searchmeliproduct.apiservice.MLService
import com.example.searchmeliproduct.factory.CategoriesViewModelFactory
import com.example.searchmeliproduct.helpers.SearchProductConstants.TYPE_SEARCH
import com.example.searchmeliproduct.helpers.SearchProductConstants.VALUE_TO_SEARCH
import com.example.searchmeliproduct.helpers.TypeSearch
import com.example.searchmeliproduct.repository.CategoryRepository
import com.example.searchmeliproduct.viewModel.CategoriesViewModel

class CategoriesFragment(private val service: MLService): Fragment() {

    private lateinit var categoryDropDownAdapter: RecyclerCategoryAdapter
    private lateinit var viewModel: CategoriesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val llContainer = inflater.inflate(R.layout.categories_fragment, container, false)

        setUpViewModel()
        setUpSpinner(llContainer)

        return llContainer
    }

    private fun setUpSpinner(container: View) {
        this.categoryDropDownAdapter = RecyclerCategoryAdapter()
        val viewManager = GridLayoutManager(this.requireContext(), 2)

        container.findViewById<RecyclerView>(R.id.rv_categories).apply {
            adapter = categoryDropDownAdapter
            layoutManager = viewManager
        }
        this.categoryDropDownAdapter.onItemClick = {category ->
            val args = Bundle()

            args.putString(VALUE_TO_SEARCH, category.id)
            args.putSerializable(TYPE_SEARCH, TypeSearch.CATEGORY)
            this.goToListProductFragment(args)
        }
    }

    private fun setUpViewModel() {
        val categoryRepository = CategoryRepository(service)
        val factory = CategoriesViewModelFactory(categoryRepository)

        viewModel = ViewModelProvider(this@CategoriesFragment, factory).get(CategoriesViewModel::class.java)
        viewModel.categories.observe(viewLifecycleOwner, Observer { categories ->
            this.categoryDropDownAdapter.categories = categories
            this.categoryDropDownAdapter.notifyDataSetChanged()
        })

    }

    private fun goToListProductFragment(args: Bundle) {
        val newFragment = ListProductFragment(service)
        val transaction = requireActivity().supportFragmentManager.beginTransaction()

        newFragment.arguments = args
        transaction.replace(R.id.containerFragment, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}