package com.example.searchmeliproduct.ui

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.searchmeliproduct.R
import com.example.searchmeliproduct.model.AttributeProduct

class TableLayoutAttributesProductFactory(private val context: Context, private val attributes: List<AttributeProduct>) {

    public fun setUpTableLayout(view: View) {
        val tableLayout = view.findViewById<TableLayout>(R.id.tl_attributesProd)
        val attributes = attributes
        var pos = 0
        var colorAttribute: Int
        var colorAttributeValue: Int

        while (pos < attributes.size) {
            colorAttribute = R.color.grey_300
            colorAttributeValue = R.color.grey_200

            if (pos % 2 == 0) {
                colorAttribute = R.color.grey_200
                colorAttributeValue = R.color.grey_100
            }

            tableLayout.addView(
                this.createTableRow(attributes[pos].name, attributes[pos].value_name, colorAttribute, colorAttributeValue),
                TableLayout.LayoutParams(
                    0,
                    TableLayout.LayoutParams.MATCH_PARENT, 1f
                )
            )

            pos++
        }
    }

    private fun createTableRow(
        attribute: String,
        value: String,
        colorAttribute: Int,
        colorAttributeValue: Int
    ): TableRow {
        val tr = TableRow(this.context)
        tr.weightSum= 1f

        val labelAttribute = this.createTextViewAttribute(attribute, colorAttribute)
        tr.addView(labelAttribute, TableRow.LayoutParams(
            0,
            TableRow.LayoutParams.MATCH_PARENT,0.5f))

        val labelAtrrValue = this.createTextViewAttribute(value, colorAttributeValue)
        tr.addView(labelAtrrValue, TableRow.LayoutParams(
            0,
            TableRow.LayoutParams.MATCH_PARENT,0.5f))

        return tr
    }

    private fun createTextViewAttribute(text:String, color: Int): TextView {
        val labelAttribute = TextView(this.context)
        val textSize = context.resources.getDimension(R.dimen.size_text_attribute)

        labelAttribute.text = text
        labelAttribute.gravity = Gravity.LEFT
        labelAttribute.textSize = textSize
        labelAttribute.setTextColor(ContextCompat.getColor(context, R.color.black))
        labelAttribute.setBackgroundColor(ContextCompat.getColor(context, color))
        labelAttribute.setPadding(60, 15, 15, 15)

        return labelAttribute
    }
}