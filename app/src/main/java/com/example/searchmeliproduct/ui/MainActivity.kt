package com.example.searchmeliproduct.ui

import android.app.Activity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.searchmeliproduct.R
import com.example.searchmeliproduct.apiservice.MLService
import com.example.searchmeliproduct.apiservice.ServiceConnector
import com.example.searchmeliproduct.helpers.SearchProductConstants.TYPE_SEARCH
import com.example.searchmeliproduct.helpers.SearchProductConstants.VALUE_TO_SEARCH
import com.example.searchmeliproduct.helpers.TypeSearch

class MainActivity : FragmentActivity() {

    private val service: MLService = ServiceConnector.connect()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.setUpBtnSearch()
        this.setUpBtnCategories()
        this.goToSearchFragment()
    }

    private fun setUpBtnSearch() {
        val btnSearch: ImageButton = this.findViewById(R.id.btn_search)

        btnSearch.setOnClickListener {
            val searchedProduct = findViewById<EditText>(R.id.ev_nameProduct).text.toString()
            val args = Bundle()

            args.putString(VALUE_TO_SEARCH, searchedProduct)
            args.putSerializable(TYPE_SEARCH, TypeSearch.NAME_PRODUCT)

            this.hideKeyboard()
            this.goToListProductFragment(args)
        }
    }

    private fun hideKeyboard() {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    private fun setUpBtnCategories() {
        this.findViewById<Button>(R.id.btn_categories).setOnClickListener {
            this.hideKeyboard()
            this.goToSearchFragment()
        }
    }

    private fun goToSearchFragment() {
        val newFragment = CategoriesFragment(service)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.containerFragment, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun goToListProductFragment(args: Bundle) {
        val newFragment = ListProductFragment(service)
        val transaction = this.supportFragmentManager.beginTransaction()

        newFragment.arguments = args
        transaction.replace(R.id.containerFragment, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val fragmentList: List<Fragment> = supportFragmentManager.fragments

        if (fragmentList.isEmpty()) {
            finish()
        }
    }
}
