package com.example.searchmeliproduct.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.searchmeliproduct.R
import com.example.searchmeliproduct.adapter.RecyclerListProductAdapter
import com.example.searchmeliproduct.apiservice.MLService
import com.example.searchmeliproduct.factory.ListProductViewModelFactory
import com.example.searchmeliproduct.helpers.SearchProductConstants.TYPE_SEARCH
import com.example.searchmeliproduct.helpers.SearchProductConstants.VALUE_TO_SEARCH
import com.example.searchmeliproduct.helpers.TypeSearch
import com.example.searchmeliproduct.repository.ProductRepository
import com.example.searchmeliproduct.viewModel.ListProductViewModel
import kotlinx.android.synthetic.main.not_found_products.*
import kotlinx.android.synthetic.main.progress_bar.*

class ListProductFragment(private val service: MLService): Fragment() {

    private lateinit var viewModel: ListProductViewModel
    private lateinit var recyclerListProductAdapter: RecyclerListProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val llContainer = inflater.inflate(R.layout.list_product_fragment, container, false)

        val progressBar = llContainer.findViewById<RelativeLayout>(R.id.progressBar)
        progressBar.visibility = View.VISIBLE
        setUpViewModel(llContainer)
        setUpRecyclerView(llContainer)

        return llContainer
    }

    private fun setUpRecyclerView(view: View) {
        recyclerListProductAdapter = RecyclerListProductAdapter()
        val viewManager = LinearLayoutManager(this.requireContext())

        recyclerListProductAdapter.onItemClick = {product ->
            val newFragment = DetailProductFragment()
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            val args = Bundle()

            args.putSerializable("productSelected", product)
            newFragment.arguments = args
            transaction.replace(R.id.containerFragment, newFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        view.findViewById<RecyclerView>(R.id.rv_products).apply {
            adapter = recyclerListProductAdapter
            layoutManager = viewManager
        }
    }

    private fun setUpViewModel(view:View) {
        val productRepository = ProductRepository(service)
        val category = this.requireArguments().getString(VALUE_TO_SEARCH).toString()
        val typeSearch = this.requireArguments().getSerializable(TYPE_SEARCH)
        val factory = ListProductViewModelFactory(productRepository, category, typeSearch as TypeSearch)

        viewModel = ViewModelProvider(this@ListProductFragment, factory).get(ListProductViewModel::class.java)
        viewModel.products.observe(viewLifecycleOwner, Observer {products ->
            recyclerListProductAdapter.products = products
            recyclerListProductAdapter.notifyDataSetChanged()
            progressBar.visibility = View.GONE
            if (products.isEmpty()) {
                view.findViewById<LinearLayout>(R.id.notFoundProductsMessage).visibility = View.VISIBLE
            }
        })
    }
}