package com.example.searchmeliproduct.model

import java.io.Serializable

class Category(val id: String, val name: String): Serializable