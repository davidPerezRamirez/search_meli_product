package com.example.searchmeliproduct.model

import java.io.Serializable

class AttributeProduct (
    val id: String,
    val value_name: String,
    val name: String
): Serializable