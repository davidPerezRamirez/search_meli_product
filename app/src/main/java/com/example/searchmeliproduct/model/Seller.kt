package com.example.searchmeliproduct.model

import java.io.Serializable

class Seller (
    val permalink: String
) : Serializable
