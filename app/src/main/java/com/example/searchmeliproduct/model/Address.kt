package com.example.searchmeliproduct.model

import java.io.Serializable

class Address (
    val state_name: String,
    val city_name: String
) : Serializable