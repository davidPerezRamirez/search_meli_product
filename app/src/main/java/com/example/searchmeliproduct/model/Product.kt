package com.example.searchmeliproduct.model

import java.io.Serializable

class Product (
    val id: String,
    val title: String,
    val price: Number,
    val thumbnail: String,
    val attributes: List<AttributeProduct>,
    val accepts_mercadopago: Boolean,
    val address: Address,
    val seller: Seller
) : Serializable