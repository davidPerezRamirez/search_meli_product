package com.example.searchmeliproduct.adapter

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.searchmeliproduct.R
import com.example.searchmeliproduct.inflate
import com.example.searchmeliproduct.model.Category

class RecyclerCategoryAdapter: RecyclerView.Adapter<RecyclerCategoryAdapter.CategoryViewHolder>(){

    companion object {
        val TAG = RecyclerListProductAdapter::class.qualifiedName
    }

    var onItemClick: ((Category) -> Unit)? = null
    var categories: List<Category> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflate = parent.inflate(R.layout.category_item,false)

        return CategoryViewHolder(inflate)
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]

        holder.bind(category)
    }

    inner class CategoryViewHolder(view: View): RecyclerView.ViewHolder(view){

        private val name: TextView = view.findViewById(R.id.tv_nameCategory)

        init {
            view.setOnClickListener {
                val category = categories[adapterPosition]

                onItemClick?.invoke(category)
                Log.i(TAG, "Se selecciono la categoria con id:" + category.id)
            }
        }

        fun bind(category: Category) {
            this.name.text = category.name
        }

    }
}