package com.example.searchmeliproduct.adapter

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.searchmeliproduct.R
import com.example.searchmeliproduct.inflate
import com.example.searchmeliproduct.model.Product
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.util.*

class RecyclerListProductAdapter: RecyclerView.Adapter<RecyclerListProductAdapter.ProductViewHolder>(){

    companion object {
        val TAG = RecyclerListProductAdapter::class.qualifiedName
    }

    var onItemClick: ((Product) -> Unit)? = null
    var products: List<Product> = emptyList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductViewHolder {
        val inflate = parent.inflate(R.layout.product_item,false)

        return ProductViewHolder(inflate)
    }

    override fun getItemCount() =  products.size

    override fun onBindViewHolder(
        holder: ProductViewHolder,
        position: Int
    ) {
        val product = products[position]

        holder.bind(product)
    }

    inner class ProductViewHolder(private val view: View): RecyclerView.ViewHolder(view){

        private val name: TextView = view.findViewById(R.id.tv_Name)
        private val price: TextView = view.findViewById(R.id.tv_price)

        init {
            view.setOnClickListener {
                val prod = products[adapterPosition]

                onItemClick?.invoke(prod)

                Log.i(TAG, "Se selecciono el producto con id:" + prod.id)
            }
        }

        fun bind(product: Product) {
            Picasso.get().load(product.thumbnail).into(view.findViewById<ImageButton>(R.id.iv_photo))
            this.name.text = product.title
            this.price.text = formatPrice(product.price)
        }

        private fun formatPrice(price: Number): String {
            val countryCode = "AR"
            val languageCoe = "es"

            return NumberFormat.getCurrencyInstance(Locale(languageCoe, countryCode)).format(price)
        }

    }

}