package com.example.searchmeliproduct.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.searchmeliproduct.helpers.TypeSearch
import com.example.searchmeliproduct.model.Product
import com.example.searchmeliproduct.repository.ProductRepository

class ListProductViewModel (
    productRepository: ProductRepository,
    valueToSearch: String,
    typeSearch: TypeSearch
) : ViewModel() {

    val products: LiveData<List<Product>> = productRepository.products(valueToSearch, typeSearch)
}