package com.example.searchmeliproduct.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.searchmeliproduct.repository.CategoryRepository
import com.example.searchmeliproduct.model.Category

class CategoriesViewModel constructor(categoryRepository: CategoryRepository): ViewModel() {

    val categories: LiveData<List<Category>> = categoryRepository.getCategories()

}