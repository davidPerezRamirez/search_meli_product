package com.example.searchmeliproduct.apiservice

import com.example.searchmeliproduct.model.Category
import com.example.searchmeliproduct.model.ListProduct
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MLService {

    @GET("categories")
    fun getCategories(): Call<List<Category>>

    @GET("search")
    fun getProductsByCategory(@Query("category") categoryId: String): Call<ListProduct>

    @GET("search")
    fun getProductsByName(@Query("q") nameProduct: String): Call<ListProduct>
}