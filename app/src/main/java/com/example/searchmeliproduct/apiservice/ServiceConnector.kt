package com.example.searchmeliproduct.apiservice

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ServiceConnector {

    companion object {
        fun connect(): MLService {
            return Retrofit.Builder()
                .baseUrl("https://api.mercadolibre.com/sites/MLA/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(MLService::class.java)
        }
    }
}