package com.example.searchmeliproduct.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.searchmeliproduct.apiservice.MLService
import com.example.searchmeliproduct.helpers.TypeSearch
import com.example.searchmeliproduct.model.ListProduct
import com.example.searchmeliproduct.model.Product
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductRepository constructor(private val service: MLService) {

    fun products(
        valueToSearch: String,
        typeSearch: TypeSearch
    ): LiveData<List<Product>> {

        val data : LiveData<List<Product>>

        if (typeSearch == TypeSearch.CATEGORY) {
            data = this.productsByCategory(valueToSearch)
        } else {
            data = this.productsByName(valueToSearch)
        }

        return data
    }

    private fun productsByCategory(idCategory: String) : LiveData<List<Product>> {
        val data = MutableLiveData<List<Product>>()

        service.getProductsByCategory(idCategory).enqueue(object : Callback<ListProduct> {
            override fun onResponse(call: Call<ListProduct>, response: Response<ListProduct>) {
                data.value = (response.body() as ListProduct).results
            }
            override fun onFailure(call: Call<ListProduct>, t: Throwable) {
                throw IllegalArgumentException("No fue posible obtener los productos para la categoria seleccionada. Causa: " + t.printStackTrace())
            }
        })
        return data
    }

    private fun productsByName(nameProduct: String ) : LiveData<List<Product>> {
        val data = MutableLiveData<List<Product>>()

        service.getProductsByName(nameProduct).enqueue(object : Callback<ListProduct> {
            override fun onResponse(call: Call<ListProduct>, response: Response<ListProduct>) {
                data.value = (response.body() as ListProduct).results
            }
            override fun onFailure(call: Call<ListProduct>, t: Throwable) {
                throw IllegalArgumentException("No fue posible obtener los productos que macheen con:" + nameProduct + ". Causa: " + t.printStackTrace())
            }
        })
        return data
    }
}