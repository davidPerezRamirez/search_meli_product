package com.example.searchmeliproduct.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.searchmeliproduct.apiservice.MLService
import com.example.searchmeliproduct.model.Category
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoryRepository (
    private val service: MLService) {

    fun getCategories(): LiveData<List<Category>> {
        val data = MutableLiveData<List<Category>>()
        service.getCategories().enqueue(object : Callback<List<Category>> {
            override fun onResponse(call: Call<List<Category>>, response: Response<List<Category>>) {
                data.value = response.body()
            }
            override fun onFailure(call: Call<List<Category>>, t: Throwable) {
                throw IllegalArgumentException("No fue posible obtener las categorias del sericio")
            }
        })
        return data
    }
}