# Acerca de proyecto

Haciendo uso de la api de [mercado libre](https://developers.mercadolibre.com.ar/es_ar/items-y-busquedas) se realizó la primera versión de la app que permite buscar artículos por cateogria y por nombre. La misma consta de 3 vistas:
- Una vista donde se pueden ver las distintas categorias que se pueden usar para realizar la búsqueda
- Una vista donde se pueden ver los items encontrados luego de la busqueda
- Una vista donde se ver la informacion de item seleccionado en la vista anterior


Adicionalmente se cuenta con una barra de búsqueda que se muestra siempre. Esta tiene un botón para poder ir a la vista de categorias desde cualquier lugar, un campo donde se puede introducir el nombre de ítem buscado y al presionar de botón de búsqueda, que se encuentra al lado, nos llevará a la vista de items encontrados.

# Acerca del desarrollo

## Herramientas

Para el desarrollo se ultilizó [Android Studio](https://developer.android.com/studio) como IDE version 3.6.3  
Para realizar pruebas funcionales e instrumentales se ultilizó el emulador [Genymotion](https://www.genymotion.com/) version 3.0.2  
Para la organización del proyecto se usó la herramienta [Trello](https://trello.com/). Acá hay un [link](https://trello.com/b/FiR6mMNt/ml-busqueda-de-producto) al scrum board del proyecto

## Diseño

Cada vista esta diseñada siguiendo el patrón de diseño MVVM(Model view ViewModel). El siguiente diagrama ilustra como esta diseñada cada vista de esta app

![Diagrama](https://drive.google.com/uc?export=view&id=1jjFo146P_TEtbbU-sCmW61N_-_MZSNyp)


## Librerias ultizadas

- Picasso para cargar imagenes
- Retrofit 2 que es un cliente Rest utilizado para conectarse a la api de MELI
- Jacoco para la creación de reportes de cobertura de código
- Mockk para los Unit Test
- Espresso para unit test e instrumentation test

## Integración continua

El proyecto cuenta con un [pipeline](https://gitlab.com/davidPerezRamirez/search_meli_product/-/blob/master/.gitlab-ci.yml) para llevar a cabo la CI. Cada vez que se realiza un push sobre master se ejecuta un job que realiza el build, ejecuta un linter y los test unitarios.

## Cobertura de código

Se usó de la librería de código abierto Jacoco. Para generar un reporte de cobertura:
1. Desde la consola de window o la terminal de android studio situarse el directorio donde esta el proyecto android
2. Ejcutar el comando: gradlew jacocoTestReport
3. Acceder al archivo: app\build\reports\jacoco\jacocoTestReport\html\index.xml


Actualmente el proyecto tiene una cobertura de codigo del 81%

## Mejoras

- Agrega funcionalidad que permita la búsqueda por cliente y por cliente y categoría
- Inyección dependencias con Dagger
  - [Documentacion revisada](https://dagger.dev/dev-guide/)
- Vinculacion de vistas, para ello se puede usar:
	- Uso de libreria [Butterknife](https://jakewharton.github.io/butterknife/) para la inyeccion de vista. Ver compatibilidad con kotlin
	- Usar [View Binding](https://developer.android.com/topic/libraries/view-binding?hl=es-419) propuesto por la documentacion oficial de android
- Configurar pipeline de CI para correr los test instrumentales
- Reemplazar RelativeLayout por ConstraintLayout

# Documentación consultada

- [Documentación oficial de android](https://developer.android.com/)
   - [Guía de arquitectura de apps](https://developer.android.com/jetpack/guide?hl=es-419)
   - [Test instrumentales usando espresso](https://developer.android.com/training/testing/espresso/lists?hl=es-419)
- [Test instrumentales usando ActivityScenario](https://medium.com/stepstone-tech/better-tests-with-androidxs-activityscenario-in-kotlin-part-1-6a6376b713ea)
- [LiveData usos y ventajas](https://sdos.es/blog/livedata-todo-sobre-el-componente-de-arquitectura-android)



